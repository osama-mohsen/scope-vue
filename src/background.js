'use strict'

import {app, protocol, ipcMain, BrowserWindow, shell} from 'electron'

const os = require("os");
const pathApp = require("path");
const fs = require("fs");

// const  setupSecureBridge = require("electron-secure-pos-printer");

// import { autoUpdater } from 'electron-updater';
import {createProtocol} from 'vue-cli-plugin-electron-builder/lib'
import installExtension, {VUEJS_DEVTOOLS} from 'electron-devtools-installer'
// const {remote} = require('electron');
// document.getElementById('help').remove();

// const getWindow = () => remote.BrowserWindow.getFocusedWindow();

//https://stackoverflow.com/questions/46866735/electron-v1-7-close-maximize-and-maximize
const isDevelopment = process.env.NODE_ENV !== 'production'

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
    {scheme: 'app', privileges: {secure: true, standard: true}}
])
let win = null

async function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 1200,
        height: 700,
        minHeight: 600,
        minWidth: 800,
        frame: true,
        icon: './public/logo.png',
        titleBarStyle: 'customButtonsOnHover',
        titleBarOverlay: true,
        useContentSize: true,
        autoHideMenuBar: true,

        webPreferences: {
            webSecurity: true,

            // Use pluginOptions.nodeIntegration, leave this alone
            // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
            nodeIntegration: true,
            contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
        }
    })
    // win.setOpacity(0.95)

    // const ses = win.webContents.session;
    //
    // // Clear cache after closing app
    // win.on('closed', () => {
    //     ses.clearStorageData({
    //         storages: ['localstorage', 'caches', 'indexdb']
    //     })
    // });


    if (process.env.WEBPACK_DEV_SERVER_URL) {
        // Load the url of the dev server if in development mode
        await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
        // if (!process.env.IS_TEST) win.webContents.openDevTools()
    } else {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
        // win.loadURL(formatUrl({
        //     pathname: path.join(__dirname, 'index.html'),
        //     protocol: 'file',
        //     slashes: true
        // }))
        // autoUpdater.checkForUpdatesAndNotify();

    }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    // if (BrowserWindow.getAllWindows().length === 0) createWindow()
    if (window === null) {
        createWindow()
    }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    if (isDevelopment && !process.env.IS_TEST) {
        // Install Vue Devtools
        try {
            await installExtension(VUEJS_DEVTOOLS)
        } catch (e) {
            console.error('Vue Devtools failed to install:', e.toString())
        }
    }
    createWindow()
})

let contentData = ""

// retransmit it to workerWindow
ipcMain.on("print", (event, content) => {
    contentData = content
    win.webContents.send("print", contentData);
});

let printerWindow = null
// let htmlData  = "<h1>welcome</h1>"
ipcMain.on("readyToPrint", (event, arg) => {
    // this.htmlData = arg
    if (printerWindow == null) {
        printerWindow = new BrowserWindow({
            width: 1000, height: 900, show: false, webPreferences: {
                webSecurity: false,

                // Use pluginOptions.nodeIntegration, leave this alone
                // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
                nodeIntegration: true,
                contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
            }
        });
        printerWindow.once('ready-to-show', () => {
            printerWindow.show()
        });
    }else{
        printerWindow.show()
    }
    let printers = win.webContents.getPrinters()
    let printer = printers.filter(printer => printer.isDefault === true)[0];
    let path = pathApp.join(__dirname, 'hello.html')
    fs.writeFile(path, contentData, function () {
        printerWindow.loadFile(path).then(r => {
            console.log(r)
        });
        printerWindow.webContents.on('did-finish-load', () => {
            const options = {
                silent: true,
                printBackground: true,
                deviceName: printer.name,
                margins: [{
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                }]
                // pageSize: {height: "3000px", width: "3000px"}
            }

            printerWindow.webContents.print(options, (isSuccess, failureReason) => {
                printerWindow.hide()
            });
        });
    })
    event.returnValue = true;
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', (data) => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}
