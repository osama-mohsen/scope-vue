import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import axios from 'axios'
import VueI18n from 'vue-i18n'
import Swal from 'sweetalert2/dist/sweetalert2.min.css'

// import Vuesax from 'vuesax'
// import loader from 'stylus-loader'
//css

//bootstrap
import { BootstrapVue, IconsPlugin ,BootstrapVueIcons } from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import '@/assets/scss/main.scss'
import "vue-modaltor/dist/vue-modaltor.css";


export const bus = new Vue()


Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(BootstrapVueIcons);
Vue.use(VueI18n);
Vue.use(Swal);
// Vue.use(Vuesax);
// Vue.use(loader)
//end bootstrap


//Global vars
// Vue.prototype.$apiUrl = 'https://obscure-sands-68445.herokuapp.com/api'
//http://localhost:8000/
// Vue.prototype.$apiUrl = "http://localhost:8000/api"
//http://192.168.130.112:8080/
// Vue.prototype.$baseUrl = "http://localhost:8000"
Vue.prototype.expireDate = 1672534800 //2023
Vue.prototype.$baseUrl = "http://localhost:8000/"
Vue.prototype.$apiUrl = Vue.prototype.$baseUrl+"api"
// Vue.prototype.$apiUrl = "http://192.168.1.199:8000/api"
Vue.prototype.$discountApi = Vue.prototype.$apiUrl+"/discount"
Vue.prototype.$axios = axios


// process.on('unhandledRejection', (error) => {
//   console.error(error)
// })

import VueAlertify from 'vue-alertify';
import vuetify from './plugins/vuetify'
import i18n from './i18n'
Vue.use(VueAlertify, {
  // notifier defaults
  notifier: {
    // auto-dismiss wait time (in seconds)
    delay: 3,
    // default position
    position: 'bottom-right',
    // adds a close button to notifier messages
    closeButton: false,
  },
});

Vue.config.productionTip = false

new Vue({
  store,
  router,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
