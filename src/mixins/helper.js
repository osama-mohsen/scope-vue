
export default {
    data: () => ({
        item: [],
        choose: [],
        clean: false,
        product_total: 0,
        product_price: 0,
        product_selected: {},
        itemsIndexWillRemove: [],
        directionLayout: "rtl"
    }),
    computed: {
        nameRules() {
            return [
                v => !!v || this.$t('this_is_field_is_required'),
                // v => (v && v.length <= 10) || 'Name must be less than 10 characters',
            ]
        },
        numberRules() {
            return [
                v => !!v || this.$t('this_is_field_is_required'),
                v => (v && v >= 1) || this.$t('this_is_field_must_have_greater_than_1')
                // v => (v && v.length <= 10) || 'Name must be less than 10 characters',
            ]
        },
        ///^[2-9]\d{2}[2-9]\d{2}\d{4}$/
        phoneRules() {
            return [
                v => !!v || this.$t('this_is_field_is_required'),
                v => (/^[0][0-1][0-5][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test(v)) || this.$t('phone_is_invalid')
                // v => (v && v.length <= 10) || 'Name must be less than 10 characters',
            ]
        },
    },
    //01152517142
    ///^[0-9]\d{2}[2-9]\d{2}\d{4}$
    methods: {
        addToCart(item, index) {//from product
            if (item.additions.length === 0) {
                item.total = item.price
                this.addToStore(item)
                this.$alertify.success(this.$t('msg.add_to_cart'));
            }
            this.$emit('productActive', index)
        },
        addToCartWithAdditions(product) {
            this.cleanCart(product)
            if (this.clean === true) {
                this.product_selected = {
                    id: product.id,
                    name: product.name,
                    price: this.product_price,
                    qty: product.qty,
                    additions: this.choose
                }
                this.product_selected.total = this.product_total
                this.addToStore(this.product_selected)
                this.$alertify.success(this.$t('msg.add_to_cart'));
                return true
            }
            return false
        },
        addToStore(item) {
            this.$store.dispatch("addToCart", {
                product: item,
                qty: 1
            })
        },
        cleanCart(product) {
            this.choose = []
            this.clean = true
            this.product_price = Number(product.price);
            this.product_total = Number(product.price)
            for (const addition of product.additions) {
                let current = {
                    id: addition.id,
                    name: addition.name,
                    price: addition.price,
                    values: []
                }
                if (addition.type === 'radio') {
                    if (addition.choose_id === null) {
                        this.$alertify.error(this.$t('you_must_choose') + addition.name);
                        this.clean = false
                        break
                    } else {
                        //choose from radio
                        addition.values.forEach(value => {
                            if (addition.choose_id === value.id) {
                                if (addition.kind === 'size') {
                                    //remove the original price , and add a selected price
                                    this.product_total -= Number(product.price);
                                    //set price to selected price
                                    this.product_price = value.price
                                }
                                //add to total price
                                this.product_total += Number(value.price)
                                let addition_value = {
                                    id: value.id,
                                    name: value.name,
                                    price: value.price,
                                    qty: value.qty
                                }
                                current.values.push(addition_value)
                            }
                        })//end values addition
                        //if selected any choose
                        if (current.values.length > 0) {
                            this.choose.push(current)
                        }
                    }//end else
                } else if (addition.type === 'checkbox') {
                    addition.values.forEach(value => {
                        if (value.is_checked === true) {
                            //add to product total
                            this.product_total += Number(value.price)
                            //add to array of selected values
                            let addition_value = {
                                id: value.id,
                                name: value.name,
                                price: value.price,
                                qty: value.qty
                            }
                            current.values.push(addition_value)
                        }
                    })//end values addition
                    //if selected any choose
                    if (current.values.length > 0) {
                        this.choose.push(current)
                    }
                }
            }
        },
        getCartItems() {
            let cart = this.$store.getters.getCart
            let isSeparate = this.$store.getters.getIsSeparate
            let items = []
            this.itemsIndexWillRemove = []
            // let itemsIndex = []
            console.log("totalCart" + cart.length)
            cart.forEach((item, index) => {
                console.log("isSeparate" + isSeparate)
                console.log("isSeparate_item" + item.is_separate)
                if (!isSeparate || (isSeparate && item.is_separate)) {
                    // items.push(item)
                    items.push(item)

                    this.itemsIndexWillRemove.push(index)
                    // this.model.items.push(item)
                }
            })
            // console.log(items)
            return items
        },
        getHeaderRequest() {
            let user = this.$store.getters.getUser
            let headers = null
            if (user != null) {
                console.log("getUser in Request Header token"+user)
                headers =
                    {
                        headers:
                            {
                                Authorization: "Bearer " + user.token,
                                // branchId: user.branch_id,
                                Accept: "application/json"
                            }
                    }
            }
            return headers
        },
        isValidResponse(response) {
            console.log("response:"+JSON.stringify(response))
            if (response.data.code !== 200) this.$alertify.error(response.data.message);
            return response.data.code === 200
        }
    }
}
