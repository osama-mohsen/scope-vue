import Vue from 'vue'
import Vuex from 'vuex'
//import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        loader: false,
        user: null,
        ItemsCounter: 0,
        msg: 'welcome in cart store',
        total: 0,
        cart: [],
        counter: 0,
        lang: 'en',
        isSeparate: false,
        separateIds: [],
        userData: {},
        print: {
            order: 1,
            kitchen: 1
        }
    },
    getters: {
        getUser: (state) => {
            console.log("getUser")
            const user = localStorage.getItem('user')
            if(user == null) return null
            if (state.user != null) return state.user

            if (user && JSON.parse(user)) return JSON.parse(user)

            return null
        },
        getPrintOrderCount(state) {
            const order = localStorage.getItem('print_order')
            state.print.order = order
            return state.print.order
        },
        getPrintKitchenCount(state) {
            const kitchen = localStorage.getItem('print_kitchen')
            state.print.kitchen = kitchen
            return state.print.kitchen
        },
        getIsSeparate(state) {
            console.log("getIsSeparate:" + state.isSeparate)
            return state.isSeparate
        },
        getCart(state) {
            let cart = state.cart
            return cart
        },
        getTotal(state) {
            let total = 0
            state.cart.forEach(item => {
                console.log("qty:" + item.qty + ",total" + item.total)
                if (!state.isSeparate || (state.isSeparate && item.is_separate))
                    total += item.qty * item.total
            })
            console.log("total:" + total)
            return total
        },
        getCartTotal(state) {
            let total = 0
            this.state.cart.map(item => {
                if (!state.isSeparate || (state.isSeparate && item.is_separate))
                    total += Number(item.price) * Number(item.qty)
            })
            state.total = total
        },
        getLanguage(state) {
            return state.lang
        }
    },
    mutations: {
        //sync
        setUser: (state, payload) => {
            console.log("set user")
            console.log("payload:"+payload.id)
            state.user = payload
            let user = {
                id: payload.id,
                branch_id : payload.branch_id,
                name: payload.name,
                email: payload.email,
                token: payload.token,
                type: payload.type
            }
            localStorage.setItem('user', JSON.stringify(user))
        },
        changeSeparate(state, {isSeparate}) {
            console.log("separate func " + isSeparate)
            state.isSeparate = isSeparate
            console.log("==>new separate:" + state.isSeparate)
        },
        addToSeparate(state, itemSeparate) {
            let pos = state.separateIds.indexOf(itemSeparate.id)
            console.log("pos:" + pos)
            state.cart.find(item => {
                if (item.id === itemSeparate.id) {
                    item.is_separate = (pos === -1)
                    if (item.is_separate) state.separateIds.push(item.id)
                    else state.separateIds.splice(pos, 1)
                }
            })
        },
        addToCart(state, {product, qty}) {
            let found = state.cart.find(item => {
                if (item.id === product.id && product.additions.length === 0) {
                    // item.qty = item.qty + 1
                    item.qty += qty
                    localStorage.setItem('cart', JSON.stringify(state.cart))
                    return true
                }
            })
            if (!found) {
                //state.cart.push(product) <----- doesn't work
                state.cart.push({
                    id: product.id,
                    name: product.name,
                    image: product.image,
                    qty: 1,
                    price: product.price,
                    category_id: product.category_id,
                    additions: product.additions,
                    total: product.total,
                    is_separate: false
                })
                localStorage.setItem('cart', JSON.stringify(state.cart))
            }

        },
        updateCart(state, {index, qty}) {
            state.cart[index].qty = qty
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        dropItem(state, index) {
            state.cart.splice(index, 1)
            localStorage.setItem('cart', JSON.stringify(this.cart))
        },
        updateOrderCount(state, count) {
            state.print.order = count
            localStorage.setItem('print_order', state.print.order)
        },
        updateKitchenCount(state, count) {
            state.print.kitchen = count
            localStorage.setItem('print_kitchen', state.print.kitchen)
        }
    },
    actions: {
        addToCart({commit}, {
            product, qty
        }) {
            commit("addToCart", {product, qty})
        },
        setUser({commit}, user) {
            console.log("set user Commit")
            commit("setUser", user)
            console.log("user:"+user.id)
        },
        changeSeparate({commit}, isSeparate) {
            console.log("action:" + isSeparate)
            commit("changeSeparate", {isSeparate})
        },
        updateCart({commit}, {
            index, qty
        }) {
            commit("updateCart", {index, qty})
        },
        clear() {
            // console.log("clear")
            if(this.state.isSeparate){
                let cartNew = []
                this.state.cart.forEach((item,index) => {
                    // console.log(item.is_separate)
                    if (!item.is_separate){
                        cartNew.push(item)
                        this.state.cart[index].is_separate = false
                    }
                })
                this.state.cart = cartNew
                this.state.isSeparate = false
                localStorage.setItem('cart', JSON.stringify(cartNew))
            }else{
                this.state.cart = []
                localStorage.setItem('cart', JSON.stringify(this.state.cart))
            }

            // localStorage.setItem('cart', JSON.stringify(this.state.cart))
        },
        dropItem({commit}, index) {
            commit("dropItem", index)
        },
        separate({commit}, isSeparate) {
            commit("separate", isSeparate)
        },
        addToSeparate({commit}, item) {
            commit("addToSeparate", item)
        },
        createOrder(response) {
            const token = localStorage.token
            const data = {
                payment_ref: response.reference,
                amount: this.cartTotal,
                user_id: this.user.id,
                items: JSON.stringify(this.cart),
                address: this.address
            }

            this.$axios.post(`${this.$apiUrl}/create-order`, data, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then(res => {
                    localStorage.removeItem('cart')
                    this.address = ''
                    this.cart = []
                    this.$alertify.success(res.data.message)
                })
                .catch(error => {
                    if (error.response.data.message) {
                        return this.$alertify.error(error.response.data.message)
                    }
                    this.$alertify.error(Object.values(error.response.data)[0][0])
                })
        },
        updateOrderCount({commit}, count) {
            commit("updateOrderCount", count)
        },
        updateKitchenCount({commit}, count) {
            commit("updateKitchenCount", count)
        },
        logout() {
            // console.log("clear")
            localStorage.setItem('user', null)
            // localStorage.setItem('cart', JSON.stringify(this.state.cart))
        },
    }
})

export default store