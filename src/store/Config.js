export const DeliveryMethods = {
    DELIVERY : "delivery",
    TAKE_AWAY : "take-away",
    TABLE : "table"
}
