module.exports = {
    css: {
        extract: true
    },

    transpileDependencies: [
        'vuetify'
    ],
    lintOnSave: false,
    pluginOptions: {
        i18n: {
            locale: 'ar',
            fallbackLocale: '\t',
            localeDir: 'locales',
            enableInSFC: false
        },

        electronBuilder: {
            nodeIntegration: true,
            builderOptions: {
                productName: "Scope",
                win: {
                    icon: './public/logo.png'
                }
            },
        },
    }
}
